fetch("http://ergast.com/api/f1/current/last/results.json")
  .then((data) => data.json())
  .then((item) => {
    const table = item;
    const races = table.MRData.RaceTable.Races[0];
    // console.log(races)
    // console.log(races['date'])
    // console.log(races['raceName'])
    // console.log(races['Results'])
    // console.log(races['Results'][0])
    // console.log(races['Results'][0]['Driver'])
    // console.log(races['Results'][0]['Driver']['driverId'])
    // console.log(races['Results'][0]['Constructor']['name'])

    $(".resultInfo").append(
      `
                    <p id="user-name" style="padding: 5px 0; text-align: center; font-size:30px">${races["raceName"]}</p>
                    <p id="user-name" style="padding: 5px 0; text-align: center; font-size:12px;">${races["date"]}</p>
                    `
    );

    for (i = 0; i < 3; i++) {
      var 템플릿 = `
                        <div class="score-card">
                            <img src="..\img\albon.avif" alt="">
                            <span style="font-size: 50px">${i + 1}</span>
                            <span style="font-size: 50px">${
                              races["Results"][i]["Driver"]["permanentNumber"]
                            }</span>
                            <p style="padding: 5px 0; font-size:28px">${
                              races["Results"][i]["Driver"]["givenName"]
                            }
                            <span style="padding: 5px 0; text-align: center; font-size:28px">${
                              races["Results"][i]["Driver"]["familyName"]
                            }</span></p>
                            <p style=" text-align: right; font-size:26px">${
                              races["Results"][i]["Constructor"]["name"]
                            }</p>
                            <p style=" text-align: right; font-size:23px">${
                              races["Results"][i]["Time"]["time"]
                            }</p>
                        </div>
                        `;

      $(".lastest-score").append(템플릿);
    }
  });
